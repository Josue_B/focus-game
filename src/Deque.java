
import java.util.Iterator;
/**
 * @author Josue
 */
public class Deque<Item> implements Iterable<Item>
{

    private ListNode<Item> linkedList;
    
      
    /*Constructor Deque. 
     * Constant time 
     */
    public Deque(){
        linkedList = new ListNode<Item>();
    }
    
    
    /*
     * return if the list is empty. 
     * Constant time.
     */
    public boolean isEmpty(){
        if (linkedList.listSize() == 0){
            return true;
        }
        return false;
        
    }
    
    
    /*
     * Add item to the front of the list. 
     * Constant time
     */
    public void addFront(Item number){
        
        //C0
        linkedList.insertFront(number);
    }
    
    
    /*
     * Add item to the back of the list.
     * Constant time.
     */
    public void addBack(Item number){
        
        //C0
        linkedList.append(number);
    }
    
    
    /*
     * Remove and return item from the front of the list.
     * C0 + C1 = Constant time.
     */
    public Item  removeFront(){
        
        //C0.
        linkedList.moveCursorToStart();
        
        //C1.
        return linkedList.removeNode();
    }
    
    
    /*
     * Remove and return the last item in the list.
     * C0 + n + C1 + C2 = O(n)
     */
    public Item removeBack(){
        
        //C0
        linkedList.moveCursorToEnd();
        
        //n
        linkedList.prev();
        
        //C1
        Item out = linkedList.removeNode();
        
        //C2
        linkedList.moveCursorToStart();
        return out ;
    }
    
    
    /*
     * Return size of the list
     * Constant time.
     */
    public int size(){
        
        //C
        return linkedList.listSize();
    }
    
    
    /*
     * Return an iterator of the list.
     * O(n)
     */
    @Override
    public Iterator<Item> iterator()
    {
        //n
        return new DequeIterator(linkedList);
        
    }
    
    
    /*
     * C0 + C1 + C2 + (N* (C2 + C3 + C4) = O(N)
     */
    @Override
    public String toString(){
        //C0
        StringBuilder str = new StringBuilder();
        //C1
        linkedList.moveCursorToStart();
        //n
        while(linkedList.getElement() != null){
            str.append(linkedList.getElement());
            //C2
            linkedList.nextNode();
            //C3
            if(linkedList.getElement() != null){
                //C4
                str.append(", ");
            }
        }
        //C2
        String outp = str.toString();
        return "<" + outp + ">";
    }

}
