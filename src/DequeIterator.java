/**
 *
 * @author Josue
 */
import java.util.Iterator;


public class DequeIterator<Integer> implements Iterator<Object>
{
    private ListNode<Integer> dequeList ;
    
    /*
     * Constructor
     * C0 + C1 = Constant time
     */
    public DequeIterator(ListNode dequeList){
        
        //C0
        this.dequeList = dequeList;
        
        //C1
        this.dequeList.moveCursorToStart();
        
    }

    /*
     * Check if the element is not null
     * Constant time
     */
    @Override
    public boolean hasNext()
    {
        //C0
        if(dequeList.getElement() != null){
            
            return true;
        }
        else{
            return false;
        }
    }

    
    /*
     * Return the item, and move to next node.
     * C0 + C1 = Constant time.
     */
    @Override
    public Object next()
    {
        //C0
        Object anObj = dequeList.getElement();
        
        //C1
        dequeList.nextNode();
        return anObj;
    }

    @Override
    public void remove()
    {
        //Not used
    }
    
}
