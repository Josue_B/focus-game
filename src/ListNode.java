/**
 *
 * @author Josue
 */
public class ListNode<E>
{
    private Node head;
    private Node tail;
    private Node cursor;
    
    private int listSize;
    
    public ListNode(){
        clear();
    }

    /*Set cursor and tail to poing to same node 
     * Head point to null Node.
     * C0 + C1 + C2= Constant Time. 
    */
    public void clear()
    {
        
        //Cursor and tail will point to a null Node. C0
        cursor = tail = new Node(null); 
        
        //Head will point to tail null node. C1
        head = new Node(tail);  
        
        //C2
        listSize = 0;
    }
    
    
    
    
    
    /*
     * Set Element where cursor is pointing.  
     * C0 + C1 + (C2 * C3) + C4 = Constant time.
     */
    public boolean insert(E element){
        
        //set next Node with the current element is. 
        //C0
        cursor.setNext(new Node(cursor.getElement(), cursor.next()));
        
        //Set new element where cursor is.
        //C1
        cursor.setElement(element);
        
        //C2
        if (tail == cursor){
            //C3
            tail = cursor.next();
        }
        //C4
        listSize++;
        return true;
    }
    
    
    /*
     * Insert an element on the front of the List.
     * C0 + C1 = Constant time.
     */
    public void insertFront(E element){
        
        //Set the cursor no the point to the firt node in List.
        //C0
        cursor = head.next;
        //C1
        insert(element);
    }
    
    
    /*
     * Append element to the end of the list.
     * C0 + C1 + C2 + C3  = Constant time.
     */
    public boolean append(E element){
        //Make a new null Node after tail. 
        //C0
        tail.setNext(new Node(null));
        
        //set element where tail is (tail points to a null Node).
        //C1
        tail.setElement(element);
        
        //Set tail to point to the new null Node we just created.
        //C2
        tail = tail.next();
        
        //Increase size.
        //C3
        listSize++;
        return true;
    }
    
    
    /*
     * Removethe node and return the 
     * element where the cursor is currently pointing to.
     * C1 + C2 + (C3 * C4) + C5 = Constant time 
     */
    public E removeNode(){
        //If the cursor is at the end null Node of List, nothing to return.
        //C0
        if (cursor == tail){
            return null;
        }
        
        //Store the element in number.
        //C1
        E number = cursor.getElement();
        
        //Set the element of the next node where cursor is.
        //C2
        cursor.setElement(cursor.next().getElement());
        
        //If the next node is where tail is, then move the tail back.
        //C3
        if (cursor.next() == tail){
            //C4
            tail = cursor;
        }
        
        //Link to next next Node.
        //C5
        cursor.setNext(cursor.next().next());
        
        //Decrease size.
        //C6
        listSize--;
        
        return number;
    }
    
    
    /*
     * Move cursor to front of List.
     * Constant time
     */
    public void moveCursorToStart(){
        //C0
        cursor = head.next();
    }
    
    /*
     * Move Cursor to end of List
     * Constant time
     */
    public void moveCursorToEnd(){
        //C0
        cursor = tail;
    }
    
    
    /*
     * Get element where cursor is pointing to.
     * Constant time.
     */
    public E getElement(){
        //C0
        return cursor.getElement();
    }
    
    
    /*
     * Get previous element.
     * C0 + C1 + (N * C2) + C3  =  O(n)
     */
    public boolean prev(){
        //if cursor is at the begginign then no previous element.
        //C0
        if (head.next() == cursor){
            return false;
        }
        //Temp node to have head value
        //C1
        Node temp = head;
        
        //Traverse list from head, and stops a node before cursor.
        //N
        while (temp.next() != cursor){
            //move temp one step to righ.
            //C2
            temp = temp.next();
        }
        //set cursor to temp. Now cursor is pointing at previous value where 
        //cursor was.
        //C3
        cursor = temp ;
        return true;
    }
    
    
    /*
     * move Cursor to nextNode in list.
     * C0 + C1 = Constant time.
     */
    public boolean nextNode(){
        //cursor cant be in the end of list.
        //C0
        if (cursor != tail){
            //move Cursor to the next element.
            //C1
            cursor = cursor.next();
            return true;
        }
        return false;
    }
    
    /*
     * Return size of list.
     * Constant time
     */
    public int listSize(){
        return this.listSize;
        
    }
    
    
    
    
    /*
     * Inner class to track nodes
     */
    private class Node{
    
    private E element ;
    private Node next;
    
    
    //Constructors
    public Node(E e, Node next){
        this.element = e ;
        this.next = next ;
    }
    public Node(Node next){
        this.next = next;
    }
    
    //Set element.
    //Constant time
    public void setElement(E e){
        this.element = e ;
    }
    
    //Get element.
    //Constant time
    public E getElement(){
        return this.element;
        
    }
    
    //Return Next node.
    //Constant time.
    public Node next(){
        return this.next;
    }
    
    //Set Next node.
    //Constant time.
    public void setNext(Node next){
        this.next = next;
    }
}
}
